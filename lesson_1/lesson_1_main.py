import random


def greet(name):
    print(f'Hello, {name}!')
    random_int = random.randint(1, 2)
    if random_int == 1:
        print('\nHave a nice day, sir!')
    else:
        print(f'\nTake a cup of tea, sir. Today will be your lucky day, {name}!')


name = input("Enter Your name: ")
greet(name)